package com.minetto.piscis_server.model.tank.dao;

import com.minetto.piscis_server.model.fish.Fish;
import com.minetto.piscis_server.model.fish.dao.FishDao;
import com.minetto.piscis_server.model.tank.Tank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
@Transactional
public class TankDaoImpl implements TankDao {
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private FishDao fishDao;
	
	@Override
	public List<Tank> getAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Tank> query = cb.createQuery(Tank.class);
		Root<Tank> root = query.from(Tank.class);
		query.select(root);
		List<Tank> tanks = entityManager.createQuery(query).getResultList();
		return tanks;
	}
	
	@Override
	public Tank getById(String id) {
		Tank tank = entityManager.find(Tank.class, id);
		if (tank != null) {
			List<Fish> fishes = fishDao.getFromTank(id);
			tank.setFishes(fishes);
		}
		return tank;
	}
	
	@Override
	public Tank editTank(Tank tank) {
		entityManager.merge(tank);
		return tank;
	}
	
	@Override
	public boolean deleteTank(String tankId) {
		Tank tank = entityManager.find(Tank.class, tankId);
		if (tank != null) {
			List<Fish> fishList = fishDao.getFromTank(tankId);
			fishList.forEach(fish -> {
				fish.setTank(null);
				entityManager.merge(fish);
			});
			entityManager.remove(entityManager.merge(tank));
			entityManager.flush();
			return true;
		}
		return false;
	}
	
	@Override
	public Tank addFish(String tankId, String fishId) {
		Fish fish = entityManager.find(Fish.class, fishId);
		Tank tank = entityManager.find(Tank.class, tankId);
		fish.setTank(tank);
		entityManager.merge(fish);
		entityManager.flush();
		entityManager.clear();
		tank.setFishes(fishDao.getFromTank(tankId));
		return tank;
	}
	
	@Override
	public Tank removeFish(String fishId) {
		Fish fish = entityManager.find(Fish.class, fishId);
		Tank tank = fish.getTank();
		if (tank != null) {
			fish.setTank(null);
			entityManager.merge(fish);
			tank.setFishes(fishDao.getFromTank(tank.getId()));
		}
		return tank;
	}
	
	@Override
	public Tank createTank(Tank tank) {
		entityManager.persist(tank);
		return tank;
	}
}
