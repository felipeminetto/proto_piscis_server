package com.minetto.piscis_server.controller;

import com.minetto.piscis_server.model.error.Error;
import com.minetto.piscis_server.model.fish.Fish;
import com.minetto.piscis_server.model.fish.KinshipLevel;
import com.minetto.piscis_server.model.fish.dao.FishDao;
import com.minetto.piscis_server.model.tank.Tank;
import com.minetto.piscis_server.model.tank.dao.TankDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class FishController {
	
	@Autowired
	FishDao fishDao;
	
	@Autowired
	private TankDao tankDao;
	
	private ResponseEntity handleException(HttpStatus status, String message) {
		Error err = new Error(status, message);
		return new ResponseEntity<>(err, err.getStatus());
	}
	
	@GetMapping("/api/fishes")
	public List<Fish> findAll() {
		return fishDao.listAll();
	}
	
	@GetMapping("/api/undiscarded_fishes")
	public List<Fish> findUndiscardeFisshes() {
		return fishDao.listUndiscarded();
	}
	
	@GetMapping("api/fish/find")
	public ResponseEntity findFish(@RequestParam(name = "fish_info") String fishInfo) {
		List<Fish> fishes = new ArrayList<>();
		Fish fish = fishDao.getByLabId(fishInfo);
		if (fish != null) {
			fishes.add(fish);
			return new ResponseEntity<>(fishes, HttpStatus.OK);
		} else {
			fishes = fishDao.getByTag(fishInfo);
			if (fishes != null && fishes.size() > 0) {
				return new ResponseEntity<>(fishes, HttpStatus.OK);
			} else {
				return handleException(HttpStatus.NOT_FOUND, "Nenhum peixe encontrado!");
			}
		}
	}
	
	@GetMapping("/api/fish")
	public ResponseEntity findFishById(@RequestParam(name = "id") String id) {
		Fish fish = fishDao.getById(id);
		if (fish != null) {
			return new ResponseEntity<>(fish, HttpStatus.OK);
		} else {
			return handleException(HttpStatus.NOT_FOUND, "Nenhum peixe cadastrado com esse ID");
		}
	}
	
	@GetMapping("/api/fish/from_tank")
	public List<Fish> findFishFromTank(@RequestParam(name = "tank_id") String tankId) {
		return fishDao.getFromTank(tankId);
	}
	
	@GetMapping("/api/fish/out_tank")
	public List<Fish> findFishOutTank(@RequestParam(name = "tank_id") String tankId) {
		return fishDao.getOutTank(tankId);
	}
	
	@PutMapping("/api/fish/edit")
	public ResponseEntity editFish(@RequestBody Fish fish) {
		if (fish.getLabId() != null && !fish.getLabId().isEmpty()) {
			Fish f = fishDao.getByLabId(fish.getLabId());
			if (f != null && !f.getId().equalsIgnoreCase(fish.getId())) {
				return handleException(HttpStatus.BAD_REQUEST, "Já existe um peixe com este número de registro!");
			}
		}
		Fish uFish = fishDao.editFish(fish);
		if (uFish != null) {
			return new ResponseEntity<>(uFish, HttpStatus.OK);
		} else {
			return handleException(HttpStatus.NOT_FOUND, "Peixe não encontrado!");
		}
	}
	
	@PostMapping("/api/fish/new")
	public ResponseEntity create(@RequestBody Fish fish) {
		if (fish.getLabId() != null && !fish.getLabId().isEmpty()) {
			Fish f = fishDao.getByLabId(fish.getLabId());
			if (f != null && !f.getId().equalsIgnoreCase(fish.getId())) {
				return handleException(HttpStatus.BAD_REQUEST, "Já existe um peixe com este número de registro!");
			}
		}
		Fish dbFishes = fishDao.getByTagUndiscarded(fish.getTag());
		if (dbFishes != null) {
			return handleException(HttpStatus.CONFLICT, "A tag selecionada ainda está em uso por um peixe ativo!");
		} else {
			Fish nFish = fishDao.create(fish);
			return new ResponseEntity<>(nFish, HttpStatus.OK);
		}
	}
	
	@PostMapping("api/fish/add_combination")
	public ResponseEntity addGeneticCombination(@RequestParam(name = "tag1") String tag1,
	                                            @RequestParam(name = "tag2") String tag2,
	                                            @RequestParam(name = "level") float level) {
		if (!tag1.equalsIgnoreCase(tag2)) {
			Fish fish1 = fishDao.getByTagUndiscarded(tag1);
			if (fish1 == null) {
				return handleException(HttpStatus.BAD_REQUEST, "Tag não pertence a um peixe cadastrado!");
			}
			Fish fish2 = fishDao.getByTagUndiscarded(tag2);
			if (fish2 == null) {
				return handleException(HttpStatus.BAD_REQUEST, "Tag não pertence a um peixe cadastrado!");
			}
			KinshipLevel match = new KinshipLevel(fish1, fish2, level);
			fishDao.addGeneticCombination(match);
			return new ResponseEntity<>(match, HttpStatus.CREATED);
		} else {
			return handleException(HttpStatus.BAD_REQUEST, "Os dois peixes não podem ser iguais!");
		}
	}
	
	@DeleteMapping("api/fish/delete_combination")
	public ResponseEntity deleteGeneticCombination(@RequestParam(name = "combination_id") String combinationId) {
		fishDao.deleteGeneticCombination(combinationId);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@GetMapping("api/fish/combination")
	public ResponseEntity<KinshipLevel> getGeneticCombination(@RequestParam("tag1") String tag1,
	                                                          @RequestParam("tag2") String tag2) {
		KinshipLevel kinshipLevel = fishDao.getGeneticMatchbination(tag1, tag2);
		if (kinshipLevel == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(kinshipLevel, HttpStatus.OK);
		}
	}
	
	@GetMapping("api/fish/relatives_for_fish")
	public List<KinshipLevel> getRelativesForFish(@RequestParam("fish_id") String fishId) {
		return fishDao.getGeneticMatchbinationsByFish(fishId);
	}
	
	@GetMapping("api/fish/matches_for_fish")
	public List<Fish> getMatchesForFish(@RequestParam("fish_id") String fishId) {
		List<KinshipLevel> matches = getRelativesForFish(fishId);
		List<Fish> fishes = new ArrayList<>();
		for (KinshipLevel match : matches) {
			if (match.getFish1().getId().equalsIgnoreCase(fishId)) {
				fishes.add(match.getFish2());
			} else {
				fishes.add(match.getFish1());
			}
		}
		return fishes;
	}
	
	@PostMapping("api/fish/discard")
	public ResponseEntity discardFish(@RequestParam(name = "fish_id") String fish_id) {
		Fish fish = fishDao.getById(fish_id);
		if (fish != null) {
			Tank tank = fish.getTank();
			if (tank != null) {
				tank.setMortality(tank.getMortality() + 1);
				tankDao.editTank(tank);
			}
			fish.setDiscarded(true);
			fishDao.editFish(fish);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return handleException(HttpStatus.NOT_FOUND, "Nenhum peixe encontrado com essa tag");
		}
	}

//	WEB   ------------
	
	@RequestMapping({"", "/fishes"})
	public ModelAndView showFishes() {
		ModelAndView model = new ModelAndView();
		model.addObject("fishes", fishDao.listAll());
		model.addObject("fish", new Fish());
		model.setViewName("fishes");
		return model;
	}
	
	@RequestMapping("/fishes/{tankId}")
	public ModelAndView showFishes(@PathVariable String tankId) {
		ModelAndView model = new ModelAndView();
		if (tankId != null && !tankId.isEmpty()) {
			model.addObject("fishes", findFishFromTank(tankId));
		} else {
			model.addObject("fishes", fishDao.listAll());
		}
		model.addObject("fish", new Fish());
		model.setViewName("fishes");
		return model;
	}
	
	@RequestMapping("/matches")
	public ModelAndView showMatches() {
		ModelAndView model = new ModelAndView();
		model.addObject("matches", fishDao.getGeneticMatchbinations());
		model.addObject("match", new Fish());
		model.setViewName("matches");
		return model;
	}
	
	@PostMapping("fish/new")
	public ModelAndView addFish(Fish fish) {
		create(fish);
		
		ModelAndView model = new ModelAndView("fishes");
		model.addObject("fishes", fishDao.listAll());
		model.addObject("fish", new Fish());
		model.addObject("fishCreated", true);
		return model;
	}
	
	@GetMapping("fish/find")
	public ModelAndView webFindFish(@RequestParam(value = "find_text") String findText) {
		List<Fish> fishes = new ArrayList<>();
		findText = findText.trim();
		Fish fish = fishDao.getByLabId(findText);
		if (fish != null) {
			fishes.add(fish);
			return showFishDetails(fish.getId());
		} else {
			fishes = fishDao.getByTag(findText);
			ModelAndView model = new ModelAndView("fishes");
			model.addObject("fish", new Fish());
			if (fishes != null && fishes.size() > 0) {
				if (fishes.size() == 1) {
					return showFishDetails(fishes.get(0).getId());
				} else {
					model.addObject("fishes", fishes);
				}
				return model;
			} else {
				model.addObject("fishes", new ArrayList<>());
			}
			return model;
		}
	}
	
	@GetMapping("fish/{fish_id}")
	public ModelAndView showFishDetails(@PathVariable("fish_id") String fishId) {
		ModelAndView model = new ModelAndView("fish_detail");
		Fish fish = fishDao.getById(fishId);
		List<KinshipLevel> relatives = getRelativesForFish(fishId);
		model.addObject("relatives", relatives);
		model.addObject("fish", fish);
		return model;
	}
	
	@PostMapping("fish/add_reprod")
	public ModelAndView addReprodDate(@RequestParam(value = "fish_id") String fishid,
	                                  @RequestParam(value = "date_reprod") String sDate) {
		Fish fish = fishDao.getById(fishid);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date date = df.parse(sDate);
			fish.getReprodutiveHistory().add(date);
			Fish uFish = fishDao.editFish(fish);
			ModelAndView model = showFishDetails(fishid);
			model.addObject("reprodSuccess", uFish != null);
			return model;
		} catch (ParseException e) {
			e.printStackTrace();
			ModelAndView model = showFishDetails(fishid);
			model.addObject("reprodSuccess", false);
			return model;
		}
	}
	
	@PostMapping("fish/discard")
	public ModelAndView webDiscardFish(@RequestParam(name = "fish_id") String fishId) {
		Fish fish = fishDao.getById(fishId);
		if (fish != null) {
			Tank tank = fish.getTank();
			if (tank != null) {
				tank.setMortality(tank.getMortality() + 1);
				tankDao.editTank(tank);
			}
			fish.setDiscarded(true);
			Fish uFish = fishDao.editFish(fish);
			ModelAndView model = showFishDetails(fishId);
			model.addObject("discardSuccess", uFish != null);
			return model;
		} else {
			ModelAndView model = showFishDetails(fishId);
			model.addObject("discardSuccess", false);
			return model;
		}
	}
	
	@PostMapping("fish/edit")
	public ModelAndView webEditFish(Fish fish,
	                                @RequestParam(value = "tank_id") String tankId,
	                                @RequestParam(value = "dateCapture") String dateCapture) {
		ModelAndView model = new ModelAndView("fish_detail");
		if (fish.getLabId() != null && !fish.getLabId().isEmpty()) {
			Fish f = fishDao.getByLabId(fish.getLabId());
			if (f != null && !f.getId().equalsIgnoreCase(fish.getId())) {
				Fish realFish = fishDao.getById(fish.getId());
				model.addObject(realFish);
				model.addObject("editSuccess", false);
				model.addObject("errorMessage", "Já existe um peixe com este número de registro!");
				return model;
			}
		}
		Tank tank = tankDao.getById(tankId);
		fish.setTank(tank);
		if (!dateCapture.isEmpty()){
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			try {
				fish.setDateCapture(df.parse(dateCapture.trim()));
			} catch (ParseException e) {
				e.printStackTrace();
				model.addObject("editSuccess", false);
				return model;
			}
		}
		Fish uFish = fishDao.editFish(fish);
		model = showFishDetails(fish.getId());
		model.addObject("editSuccess", uFish != null);
		return model;
	}
	
	@GetMapping("fish/move_to")
	public ModelAndView webMoveFishToTank(@RequestParam(value = "fish_id") String fishId,
	                                      @RequestParam(value = "tank_id") String tankId) {
		Fish fish = fishDao.getById(fishId);
		Tank tank = tankDao.getById(tankId);
		fish.setTank(tank);
		Fish uFish = fishDao.editFish(fish);
		ModelAndView model = showFishDetails(fish.getId());
		model.addObject("moveSuccess", uFish != null);
		return model;
	}
}
