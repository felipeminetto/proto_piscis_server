package com.minetto.piscis_server.model.fish;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "kinship_level")
public class KinshipLevel {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	@OneToOne(orphanRemoval = true)
	@JoinColumn(name = "fish1_id")
	private Fish fish1;
	
	@OneToOne(orphanRemoval = true)
	@JoinColumn(name = "fish2_id")
	private Fish fish2;
	
	private float level;
	
	public KinshipLevel() {
	}
	
	public KinshipLevel(Fish fish1, Fish fish2, float level) {
		this.fish1 = fish1;
		this.fish2 = fish2;
		this.level = level;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public float getLevel() {
		return level;
	}
	
	public void setLevel(float level) {
		this.level = level;
	}
	
	public Fish getFish1() {
		return fish1;
	}
	
	public void setFish1(Fish fish1) {
		this.fish1 = fish1;
	}
	
	public Fish getFish2() {
		return fish2;
	}
	
	public void setFish2(Fish fish2) {
		this.fish2 = fish2;
	}
}
