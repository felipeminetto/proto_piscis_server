package com.minetto.piscis_server.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.minetto.piscis_server.model.user.User;
import com.minetto.piscis_server.model.user.dao.UserDao;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.minetto.piscis_server.config.security.SecurityConstants.*;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDao userDao;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req,
	                                            HttpServletResponse res) throws AuthenticationException {
		try {
			User creds = new ObjectMapper()
					.readValue(req.getInputStream(), User.class);
			
			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							creds.getUsername(),
							creds.getPassword(),
							new ArrayList<>())
			);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
	                                        Authentication auth) throws IOException, ServletException {
		
		Date expirationDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
		String userName = ((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername();
		User user = userDao.getUserByEmail(userName);
		String token = Jwts.builder()
				.setSubject(userName)
				.setExpiration(expirationDate)
				.signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
				.compact();
		user.setToken(token);
		user.setTokenExpiration(expirationDate);
		userDao.editUser(user);
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
	}
}
