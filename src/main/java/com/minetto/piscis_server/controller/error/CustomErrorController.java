package com.minetto.piscis_server.controller.error;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.GetMapping;

public class CustomErrorController implements ErrorController {
	
	private static final String PATH = "/error";
	
	@GetMapping(value = PATH)
	public String error() {
		return "Error haven";
	}
	
	@Override
	public String getErrorPath() {
		return PATH;
	}
}