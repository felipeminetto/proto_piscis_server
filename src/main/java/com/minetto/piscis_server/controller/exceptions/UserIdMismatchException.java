package com.minetto.piscis_server.controller.exceptions;

public class UserIdMismatchException extends RuntimeException {
	
	public UserIdMismatchException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UserIdMismatchException() {
	
	}
}
