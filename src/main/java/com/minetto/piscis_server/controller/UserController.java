package com.minetto.piscis_server.controller;

import com.minetto.piscis_server.model.error.Error;
import com.minetto.piscis_server.model.user.User;
import com.minetto.piscis_server.model.user.dao.UserDao;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.minetto.piscis_server.config.security.SecurityConstants.*;

@RestController
public class UserController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserDao userDao;
	
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	
	private ResponseEntity handleException(HttpStatus status, String message) {
		Error err = new Error(status, message);
		return new ResponseEntity<>(err, err.getStatus());
	}
	
	@GetMapping("/api/users")
	public List<User> findAll() {
		return userDao.getUsers();
	}
	
	@PostMapping(SIGN_IN_URL)
	public ResponseEntity login(@RequestParam(name = "username") String username,
	                            @RequestParam(name = "password") String password) {
		
		User user = null;
		try {
			user = userDao.getUserByEmail(username);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		if (user == null) {
			return handleException(HttpStatus.UNAUTHORIZED, "E-mail ou senha inválido!");
		} else {
			if (bCryptPasswordEncoder.matches(password, user.getPassword())) {
				
				Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password, new ArrayList<>()));
				String token = Jwts.builder()
						.setSubject(((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername())
						.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
						.signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
						.compact();
				
				user.setPassword(null);
				user.setToken(token);
				return new ResponseEntity<>(user, HttpStatus.OK);
			} else {
				return handleException(HttpStatus.UNAUTHORIZED, "E-mail ou senha inválido!");
			}
		}
	}
	
	@GetMapping("/api/users/name")
	public List<User> findUsersByName(@RequestParam String name) {
		return userDao.getUserByName(name);
	}
	
	@PostMapping("/api/user/change_password")
	public ResponseEntity changePassword(@RequestParam(name = "user_id") String userId,
	                                     @RequestParam(name = "old_pwd") String oldPwd,
	                                     @RequestParam(name = "new_pwd") String newPwd) {
		User user = userDao.getUserById(userId);
		if (bCryptPasswordEncoder.matches(oldPwd, user.getPassword())) {
			user.setPassword(bCryptPasswordEncoder.encode(newPwd));
			userDao.editUser(user);
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return handleException(HttpStatus.UNAUTHORIZED, "Senha antiga inválida!");
		}
	}
	
	@PostMapping("/api/user/change_email")
	public ResponseEntity changeEmail(@RequestParam(name = "user_id") String userId,
	                                  @RequestParam(name = "old_pwd") String oldPwd,
	                                  @RequestParam(name = "email") String email) {
		User user = userDao.getUserById(userId);
		User dbUser = userDao.getUserByEmail(email);
		if (dbUser == null) {
			if (bCryptPasswordEncoder.matches(oldPwd, user.getPassword())) {
				user.setUsername(email);
				userDao.editUser(user);
				return new ResponseEntity<>(user, HttpStatus.OK);
			} else {
				return handleException(HttpStatus.UNAUTHORIZED, "Senha inválida!");
			}
		} else {
			return handleException(HttpStatus.BAD_REQUEST, "Já existe um usuário com esse e-mail!");
		}
	}
	
	@DeleteMapping("api/user/delete/{user_id}")
	public ResponseEntity deleteUser(@PathVariable("user_id") String userId) {
		if (userDao.deleteUser(userId)) {
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return handleException(HttpStatus.NOT_FOUND, "Usuário não encontrado.");
		}
	}
	
	@PostMapping(SIGN_UP_URL)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity create(@RequestBody User user) {
		if (user.getUsername() != null && !user.getUsername().isEmpty()) {
			User dbUser = userDao.getUserByEmail(user.getUsername());
			if (dbUser == null) {
				user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
				return new ResponseEntity<>(userDao.create(user), HttpStatus.OK);
			} else {
				return handleException(HttpStatus.BAD_REQUEST, "Já existe um usuário com esse e-mail!");
			}
		} else {
			return handleException(HttpStatus.BAD_REQUEST, "O usuário deve ter um e-mail");
		}
	}
	
	//	WEB   ------------
	@RequestMapping("/users")
	public ModelAndView showUsers() {
		ModelAndView model = new ModelAndView("users");
		model.addObject("users", findAll());
		model.addObject("user", new User());
		return model;
	}
	
	@PostMapping("user/new")
	public ModelAndView addUser(User user) {
		boolean created = false;
		if (user.getUsername() != null && !user.getUsername().isEmpty()) {
			User dbUser = userDao.getUserByEmail(user.getUsername());
			if (dbUser == null) {
				user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
				userDao.create(user);
				created = true;
			}
		}
		
		ModelAndView model = new ModelAndView("users");
		model.addObject("users", userDao.getUsers());
		model.addObject("user", new User());
		model.addObject("userCreated", created);
		return model;
	}
	
	@PostMapping("/user/delete")
	public ModelAndView webDeleteUser(@RequestParam String userId) {
		if (userDao.deleteUser(userId)) {
			ModelAndView model = showUsers();
			model.addObject("userDeleted", true);
			return model;
		} else {
			ModelAndView model = showUsers();
			model.addObject("userDeleted", false);
			return model;
		}
	}
}
