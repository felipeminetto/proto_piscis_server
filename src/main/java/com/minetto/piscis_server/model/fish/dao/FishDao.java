package com.minetto.piscis_server.model.fish.dao;

import com.minetto.piscis_server.model.fish.Fish;
import com.minetto.piscis_server.model.fish.KinshipLevel;

import java.util.List;

public interface FishDao {
	
	List<Fish> listAll();
	
	List<Fish> listUndiscarded();
	
	Fish getById(String id);
	
	List<Fish> getFromTank(String tankId);
	
	List<Fish> getOutTank(String tankId);
	
	Fish create(Fish fish);
	
	List<Fish> getAllByTag(String tag);
	
	Fish editFish(Fish fish);
	
	void addGeneticCombination(KinshipLevel kinshipLevel);
	
	boolean deleteGeneticCombination(String combinationId);
	
	List<KinshipLevel> getGeneticMatchbinations();
	
	List<KinshipLevel> getGeneticMatchbinationsByFish(String fishId);
	
	KinshipLevel getGeneticMatchbination(String tag1, String tag2);
	
	List<Fish> getByTag(String tag);
	
	Fish getByTagUndiscarded(String tag);
	
	void updateKinshipLevel(KinshipLevel level);
	
	Fish getByLabId(String labId);
}
