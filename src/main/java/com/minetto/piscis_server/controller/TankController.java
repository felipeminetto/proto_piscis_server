package com.minetto.piscis_server.controller;

import com.minetto.piscis_server.model.error.Error;
import com.minetto.piscis_server.model.fish.Fish;
import com.minetto.piscis_server.model.fish.dao.FishDao;
import com.minetto.piscis_server.model.tank.Tank;
import com.minetto.piscis_server.model.tank.dao.TankDao;
import com.minetto.piscis_server.model.user.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class TankController {
	
	@Autowired
	private TankDao tankDao;
	
	@Autowired
	private FishDao fishDao;
	
	@Autowired
	private UserDao userDao;
	
	private ResponseEntity handleException(HttpStatus status, String message) {
		Error err = new Error(status, message);
		return new ResponseEntity<>(err, err.getStatus());
	}
	
	@GetMapping("api/tanks")
	public List<Tank> findAll() {
		List<Tank> tanks = tankDao.getAll();
		
		for (Tank tank : tanks) {
			tank.setFishes(fishDao.getFromTank(tank.getId()));
			tank.setSpecies(getSpeciesFromFishes(tank.getFishes()));
		}
		return tanks;
	}
	
	private String getSpeciesFromFishes(List<Fish> fishes) {
		String species = "";
		for (Fish fish : fishes) {
			if (fish.getSpecies() != null && !species.contains(fish.getSpecies())) {
				species = String.format("%s, %s", species, fish.getSpecies());
			}
		}
		if (species.length() > 1) {
			species = species.replaceFirst(", ", "");
		}
		return species;
	}
	
	@GetMapping("api/tank")
	public Tank findById(@RequestParam(name = "id") String tankId) {
		return tankDao.getById(tankId);
	}
	
	@PutMapping("api/tank/edit")
	public Tank updateTank(@RequestBody Tank tank) {
		return tankDao.editTank(tank);
	}
	
	@PostMapping("api/tank/add_fish")
	public ResponseEntity addFishToTank(@RequestParam(name = "fish_id") String fishId,
	                                    @RequestParam(name = "tank_id") String tankId) {
		
		Tank tank = tankDao.addFish(tankId, fishId);
		if (tank != null) {
			return new ResponseEntity<>(tank, HttpStatus.OK);
		} else {
			return handleException(HttpStatus.BAD_REQUEST, "Não foi possível adicionar o peixe!");
		}
	}
	
	@PostMapping("api/tank/add_fishes")
	public ResponseEntity addFishesToTank(@RequestBody Tank reqTank) {
		Tank tank = tankDao.getById(reqTank.getId());
		if (tank != null) {
			for (Fish f : reqTank.getFishes()) {
				Fish fish = fishDao.getById(f.getId());
				fish.setTank(tank);
				fishDao.editFish(fish);
			}
			tank = tankDao.getById(reqTank.getId());
			return new ResponseEntity<>(tank, HttpStatus.OK);
		} else {
			return handleException(HttpStatus.BAD_REQUEST, "Não foi possível adicionar os peixes!");
		}
	}
	
	@PostMapping("api/tank/remove_fish")
	public ResponseEntity removeFishFromTank(@RequestParam(name = "fish_id") String fishId) {
		Tank tank = tankDao.removeFish(fishId);
		if (tank != null) {
			return new ResponseEntity<>(tank, HttpStatus.OK);
		} else {
			return handleException(HttpStatus.NOT_FOUND, "Nenhum tanque possui este peixe.");
		}
	}
	
	@PostMapping("api/tank/new")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity create(@RequestBody Tank tank) {
		if (tank != null) {
			Tank newTank = tankDao.createTank(tank);
			return new ResponseEntity<>(newTank, HttpStatus.OK);
		} else {
			return handleException(HttpStatus.BAD_REQUEST, "Não possível cadastrar o tanque!");
		}
	}
	
	@DeleteMapping("api/tank/delete/{tank_id}")
	public ResponseEntity deleteTank(@PathVariable("tank_id") String tank_id) {
		List<Fish> fishList = fishDao.getFromTank(tank_id);
		if (fishList != null) {
			for (Fish fish : fishList) {
				tankDao.removeFish(fish.getId());
			}
		}
		
		if (tankDao.deleteTank(tank_id)) {
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return handleException(HttpStatus.NOT_FOUND, "Tanque não encontrado.");
		}
	}

//	WEB   ------------
	
	@RequestMapping("/tanks")
	public ModelAndView showTanks() {
		ModelAndView model = new ModelAndView("tanks");
		List<Tank> tanks = findAll();
		for (Tank tank : tanks) {
			tank.setSpecies(getSpeciesFromFishes(tank.getFishes()));
		}
		model.addObject("tanks", findAll());
		model.addObject("tank", new Tank());
		return model;
	}
	
	@PostMapping("tank/new")
	public ModelAndView addTank(Tank tank) {
		create(tank);
		
		ModelAndView model = new ModelAndView("tanks");
		model.addObject("tanks", tankDao.getAll());
		model.addObject("tank", new Tank());
		model.addObject("tankCreated", true);
		return model;
	}
	
	@PostMapping("tank/edit")
	public ModelAndView webEditTank(Tank tank) {
		Tank dbTank = tankDao.getById(tank.getId());
		if (dbTank != null){
			dbTank.setName(tank.getName());
			dbTank.setDescription(tank.getDescription());
			Tank nTank = tankDao.editTank(dbTank);
			
			ModelAndView model = new ModelAndView("tank_detail");
			model.addObject("tank", nTank);
			model.addObject("editSuccess", true);
			return model;
		} else {
			ModelAndView model = new ModelAndView("tanks");
			model.addObject("tank", tank);
			model.addObject("editSuccess", false);
			return model;
		}
	}
	
	@GetMapping("/tank/{tank_id}")
	public ModelAndView tankDetails(@PathVariable("tank_id") String tankId) {
		ModelAndView model = new ModelAndView("tank_detail");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth.getPrincipal() instanceof User) {
			String userId = ((User) auth.getPrincipal()).getUsername();
			com.minetto.piscis_server.model.user.User user = userDao.getUserById(userId);
			model.addObject("user", user);
		}
		Tank tank = findById(tankId);
		tank.setFishes(fishDao.getFromTank(tankId));
		model.addObject("tank", tank);
		return model;
	}
	
	@GetMapping("/tank/delete")
	public ModelAndView webDeleteTank(@RequestParam("tank_id") String tankId) {
		List<Fish> fishList = fishDao.getFromTank(tankId);
		if (fishList != null) {
			for (Fish fish : fishList) {
				tankDao.removeFish(fish.getId());
			}
		}
		
		if (tankDao.deleteTank(tankId)) {
			ModelAndView model = showTanks();
			model.setViewName("tanks");
			model.addObject("removedTank", true);
			return model;
		} else {
			ModelAndView model = tankDetails(tankId);
			model.addObject("errorDelete", true);
			return model;
		}
	}
	
	@GetMapping("/tank/{tank_id}/select_fishes/")
	public ModelAndView selectFishes(@PathVariable("tank_id") String tankId) {
		Tank tank = tankDao.getById(tankId);
		if (tank != null) {
			ModelAndView model = new ModelAndView("select_fishes");
			model.addObject("tank", tank);
			model.addObject("fishes", fishDao.getOutTank(tankId));
			return model;
		} else {
			ModelAndView model = showTanks();
			model.addObject("selectFishError", true);
			return model;
		}
	}
	
	@PostMapping("/tank/add_fishes")
	public ModelAndView webAddFishesToTank(@RequestParam("tank_id") String tankId,
	                                       @RequestParam("fishSelected") List<String> fishIdSelected) {
		Tank tank = tankDao.getById(tankId);
		if (tank != null) {
			for (String fishId : fishIdSelected) {
				Fish fish = fishDao.getById(fishId);
				fish.setTank(tank);
				fishDao.editFish(fish);
			}
			ModelAndView model = tankDetails(tankId);
			model.addObject("fishAddedSuccess", true);
			return model;
		} else {
			ModelAndView model = tankDetails(tankId);
			model.addObject("fishAddedSuccess", false);
			return model;
		}
	}
	
	@GetMapping("fish/{fish_id}/move")
	public ModelAndView webMoveFish(@PathVariable(value = "fish_id") String fishId) {
		ModelAndView model = new ModelAndView("tanks");
		Fish fish = fishDao.getById(fishId);
		List<Tank> tanks = tankDao.getAll();
		for (Tank tank : tanks) {
			tank.setFishes(fishDao.getFromTank(tank.getId()));
			tank.setSpecies(getSpeciesFromFishes(tank.getFishes()));
		}
		model.addObject("fish", fish);
		model.addObject("tanks", tanks);
		return model;
	}
}
