package com.minetto.piscis_server.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.annotation.Resource;

import static com.minetto.piscis_server.config.security.SecurityConstants.SIGN_IN_URL;
import static com.minetto.piscis_server.config.security.SecurityConstants.SIGN_UP_URL;


@EnableWebSecurity
public class MultiHttpSecurityConfig {
	
	
	@Order(1)
	@Configuration
	public static class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {
		
		@Resource(name = "userDaoImpl")
		private UserDetailsService userDetailsService;
		
		@Override
		@Bean
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
					.antMatcher("/api/**")
					.authorizeRequests()
					.anyRequest().authenticated()
					.and()
					.addFilter(new JWTAuthenticationFilter(authenticationManager()))
					.addFilter(new JWTAuthorizationFilter(authenticationManager()))
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}
		
		@Override
		public void configure(WebSecurity web) throws Exception {
			web.ignoring()
					.antMatchers(SIGN_UP_URL)
					.antMatchers(SIGN_IN_URL);
		}
		
		@Override
		public void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		}
		
		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder();
		}
	}
	
	@Configuration
	public static class FormLoginWebSecurityConfig extends WebSecurityConfigurerAdapter {
		
		@Resource(name = "userDaoImpl")
		private UserDetailsService userDetailsService;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf()
					.requireCsrfProtectionMatcher(new AntPathRequestMatcher("login"))
					.and()
					.authorizeRequests()
					.antMatchers("/", "/fish/**", "/tank/**", "/user/**", "/fishes/**", "/tanks/**", "/users/**")
					.hasRole("USER")
					.and()
					.formLogin()
					.loginPage("/login")
					.defaultSuccessUrl("/")
					.and()
					.logout()
					.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/login");
		}
		
		@Override
		public void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		}
		
		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder();
		}
		
		@Override
		public void configure(WebSecurity web) {
			web.ignoring().antMatchers("/*.css");
			web.ignoring().antMatchers("/*.js");
		}
	}
	
}