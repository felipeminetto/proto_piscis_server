package com.minetto.piscis_server.model.user.dao;

import com.minetto.piscis_server.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;

@Component
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public List<User> getUsers() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> query = cb.createQuery(User.class);
		Root<User> root = query.from(User.class);
		query.select(root);
		return entityManager.createQuery(query).getResultList();
	}
	
	@Override
	@Transactional
	public List<User> getUserByName(String name) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> query = cb.createQuery(User.class);
		Root<User> root = query.from(User.class);
		query.where(cb.like(root.get("name"), "%" + name + "%"));
		return entityManager.createQuery(query).getResultList();
	}
	
	@Override
	@Transactional
	public User getUserByEmail(String email) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> query = cb.createQuery(User.class);
		Root<User> root = query.from(User.class);
		query.where(cb.equal(root.get("username"), email));
		List<User> resultList = entityManager.createQuery(query).getResultList();
		if (resultList.size() > 0)
			return resultList.get(0);
		else
			return null;
	}
	
	@Override
	@Transactional
	public User getUserById(String id) {
		return entityManager.find(User.class, id);
	}
	
	@Override
	@Transactional
	public User create(User user) {
		entityManager.persist(user);
		return user;
	}
	
	@Override
	@Transactional
	public void editUser(User user) {
		User dbUser = entityManager.find(User.class, user.getId());
		if (dbUser != null) {
			dbUser.setPassword(user.getPassword());
			dbUser.setUsername(user.getUsername());
			entityManager.merge(dbUser);
			entityManager.flush();
		}
	}
	
	private List<SimpleGrantedAuthority> getAuthority() {
		return Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = getUserByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		} else {
			return new org.springframework.security.core.userdetails.User(user.getId(), user.getPassword(), getAuthority());
		}
	}
	
	@Override
	@Transactional
	public boolean deleteUser(String userId) {
		User user = entityManager.find(User.class, userId);
		if (user != null) {
			entityManager.remove(user);
			entityManager.flush();
			return true;
		}
		return false;
	}
}
