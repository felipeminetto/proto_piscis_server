package com.minetto.piscis_server.model.tank;

import com.minetto.piscis_server.model.fish.Fish;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Tank {
	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	private String name;
	
	private int mortality;
	
	private String description;
	
	@Transient
	private List<Fish> fishes;
	
	@Transient
	private String species;
	
	public Tank() {
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getMortality() {
		return mortality;
	}
	
	public void setMortality(int mortality) {
		this.mortality = mortality;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Fish> getFishes() {
		return fishes;
	}
	
	public void setFishes(List<Fish> fishes) {
		this.fishes = fishes;
	}
	
	public String getSpecies() {
		return species;
	}
	
	public void setSpecies(String species) {
		this.species = species;
	}
}
