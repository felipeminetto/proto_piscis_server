package com.minetto.piscis_server.controller;

import com.minetto.piscis_server.model.fish.Fish;
import com.minetto.piscis_server.model.fish.KinshipLevel;
import com.minetto.piscis_server.model.fish.dao.FishDao;
import com.minetto.piscis_server.storage.StorageFileNotFoundException;
import com.minetto.piscis_server.storage.StorageService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FileUploadController {
	
	private final StorageService storageService;
	
	@Autowired
	private FishDao fishDao;
	
	@Autowired
	public FileUploadController(StorageService storageService) {
		this.storageService = storageService;
	}
	
	@GetMapping("/upload")
	public String showUploadPage(Model model) throws IOException {
		model.addAttribute("message", "Escolha a planilha para ser importada.");
		return "upload";
	}
	
	@PostMapping("/upload")
	public String handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		String[] fileParts = file.getOriginalFilename().split("\\.");
		String extension = fileParts[fileParts.length - 1];
		if (extension.contains("xls")) {
			String filePath = storageService.store(file);
			processFile(filePath);
			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded " + file.getOriginalFilename() + "!");
		} else {
			redirectAttributes.addFlashAttribute("error", "Formato de arquivo não aceito!\nUtilize apenas arquivos .xls ou .xlsx.");
		}
		return "redirect:/upload";
	}
	
	private void processFile(String filePath) {
		File file = new File(filePath);
		try {
			Workbook workbook = WorkbookFactory.create(file);
			Sheet sheet = workbook.getSheetAt(0);
			String sheetName = sheet.getSheetName();
			int fishCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
			Row listOfFishes = sheet.getRow(sheet.getFirstRowNum()); //apartir do num 1
			List<Fish> fishes = new ArrayList<>();
			for (int i = 1; i <= fishCount; i++) {
				Cell cell = listOfFishes.getCell(i);
				fishes.add(fishDao.getByLabId(getCellValue(cell, cell.getCellTypeEnum())));
			}
			for (int i = 1; i <= fishCount; i++) {
				for (int j = 1; j <= fishCount; j++) {
					Cell cell = sheet.getRow(i).getCell(j);
					if (cell != null) {
						Fish fish1 = fishes.get(i - 1);
						Fish fish2 = fishes.get(j - 1);
						KinshipLevel level;
						if (fish1.getGender() == 1) {
							level = fishDao.getGeneticMatchbination(fish1.getTag(), fish2.getTag());
						} else {
							level = fishDao.getGeneticMatchbination(fish2.getTag(), fish1.getTag());
						}
						if (level == null) {
							KinshipLevel kinshipLevel = new KinshipLevel(fish1, fish2, (float) cell.getNumericCellValue());
							fishDao.addGeneticCombination(kinshipLevel);
						} else {
							level.setLevel((float) cell.getNumericCellValue());
							fishDao.updateKinshipLevel(level);
						}
					}
					System.out.print("\t");
				}
				System.out.print("\n");
			}
			workbook.close();
			file.delete();
		} catch (IOException | InvalidFormatException e) {
			e.printStackTrace();
		}
	}
	
	private String getCellValue(Cell cell, CellType cellType) {
		switch (cellType) {
			case STRING:
				return cell.getStringCellValue();
			case NUMERIC:
				return String.format("%.0f", cell.getNumericCellValue());
			case FORMULA:
				return getCellValue(cell, cell.getCachedFormulaResultTypeEnum());
			default:
				return "";
		}
	}
	
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}
}
