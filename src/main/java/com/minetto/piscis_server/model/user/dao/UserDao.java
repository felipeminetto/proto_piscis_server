package com.minetto.piscis_server.model.user.dao;

import com.minetto.piscis_server.model.user.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserDao extends UserDetailsService {
	
	List<User> getUsers();
	
	User getUserById(String id);
	
	List<User> getUserByName(String name);
	
	User getUserByEmail(String email);
	
	User create(User user);
	
	void editUser(User user);
	
	boolean deleteUser(String userId);
}
