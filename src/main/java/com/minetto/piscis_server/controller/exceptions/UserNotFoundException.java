package com.minetto.piscis_server.controller.exceptions;

public class UserNotFoundException extends RuntimeException {
	
	public UserNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UserNotFoundException() {
	
	}
}
