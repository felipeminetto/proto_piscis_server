package com.minetto.piscis_server.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.minetto.piscis_server.model")
@EntityScan("com.minetto.piscis_server.model")
@ComponentScan(basePackages = "com.minetto.piscis_server")
@SpringBootApplication
public class PiscisServerApplication extends SpringBootServletInitializer {
	
	private static Class applicationClass = PiscisServerApplication.class;
	
	public static void main(String[] args) {
		SpringApplication.run(PiscisServerApplication.class, args);
	}
}
