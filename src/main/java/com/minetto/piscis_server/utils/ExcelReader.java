package com.minetto.piscis_server.utils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;

public class ExcelReader {
	public static final String SAMPLE_XLSX_FILE_PATH = "/src/main/resources/raw/Análise Cruzamentos  Felipe.xlsx";
	
	public static void main(String[] args) throws IOException, InvalidFormatException {
		
		// Creating a Workbook from an Excel file (.xls or .xlsx)
		String appPath = System.getProperty("user.dir");
		Workbook workbook = WorkbookFactory.create(new File(appPath + SAMPLE_XLSX_FILE_PATH));
		
		// Retrieving the number of sheets in the Workbook
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

        /*
           =============================================================
           Iterating over all the sheets in the workbook (Multiple ways)
           =============================================================
        */
			
		System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});

        /*
           ==================================================================
           Iterating over all the rows and columns in a Sheet (Multiple ways)
           ==================================================================
        */
		
		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);
		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();
		
		System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
		sheet.forEach(row -> {
			row.forEach(cell -> {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
			});
			System.out.println();
		});
		
		System.out.println("\n\nIterating over Rows and Columns and printing the cell content accordingly to its type\n");
		sheet.forEach(row -> {
			row.forEach(cell -> {
				printCellValue(cell, cell.getCellTypeEnum());
			});
			System.out.println();
		});
		
		// Closing the workbook
		workbook.close();
	}
	
	private static void printCellValue(Cell cell, CellType cellType) {
		switch (cellType) {
			case BOOLEAN:
				System.out.print(cell.getBooleanCellValue());
				break;
			case STRING:
				System.out.print(cell.getRichStringCellValue().getString());
				break;
			case NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					System.out.print(cell.getDateCellValue());
				} else {
					System.out.print(cell.getNumericCellValue());
				}
				break;
			case FORMULA:
				printCellValue(cell, cell.getCachedFormulaResultTypeEnum());
				break;
			case BLANK:
				System.out.print("");
				break;
			default:
				System.out.print("");
		}
		
		System.out.print("\t");
	}
}
