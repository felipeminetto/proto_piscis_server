package com.minetto.piscis_server.model.fish.dao;

import com.minetto.piscis_server.model.fish.Fish;
import com.minetto.piscis_server.model.fish.KinshipLevel;
import com.minetto.piscis_server.model.tank.Tank;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class FishDaoImpl implements FishDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Fish create(Fish fish) {
		entityManager.persist(fish);
		return fish;
	}
	
	@Override
	public List<Fish> listAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
		Root<Fish> root = query.from(Fish.class);
		query.select(root);
		return entityManager.createQuery(query).getResultList();
	}
	
	@Override
	public List<Fish> listUndiscarded() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
		Root<Fish> root = query.from(Fish.class);
		query.where(cb.isFalse(root.get("discarded")));
		return entityManager.createQuery(query).getResultList();
	}
	
	@Override
	public Fish getByLabId(String labId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
		Root<Fish> root = query.from(Fish.class);
		query.where(cb.equal(root.get("labId"), labId));
		try {
			return entityManager.createQuery(query).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public Fish getById(String id) {
		return entityManager.find(Fish.class, id);
	}
	
	@Override
	public List<Fish> getByTag(String tag) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
		Root<Fish> root = query.from(Fish.class);
		query.where(cb.like(root.get("tag"), "%" + tag + "%"));
		try {
			return entityManager.createQuery(query).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public Fish getByTagUndiscarded(String tag) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
		Root<Fish> root = query.from(Fish.class);
		query.where(cb.and(
				cb.like(root.get("tag"), "%" + tag + "%"),
				cb.isFalse(root.get("discarded")))
		);
		try {
			return entityManager.createQuery(query).getResultList().get(0);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Fish> getAllByTag(String tag) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
		Root<Fish> root = query.from(Fish.class);
		query.where(cb.equal(root.get("tag"), tag));
		try {
			return entityManager.createQuery(query).getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public Fish editFish(Fish fish) {
		Fish dbFish = entityManager.find(Fish.class, fish.getId());
		if (dbFish != null) {
			dbFish.setGender(fish.getGender());
			dbFish.setTag(fish.getTag());
			dbFish.setSize(fish.getSize());
			dbFish.setWeight(fish.getWeight());
			dbFish.setTank(fish.getTank());
			dbFish.setLocality(fish.getLocality());
			dbFish.setLabId(fish.getLabId());
			dbFish.setObservation(fish.getObservation());
			dbFish.setSpecies(fish.getSpecies());
			dbFish.setDateCapture(fish.getDateCapture());
			dbFish.setDiscarded(fish.isDiscarded());
			dbFish.setReprodutiveHistory(fish.getReprodutiveHistory());
			entityManager.merge(dbFish);
			entityManager.flush();
		}
		return dbFish;
	}
	
	@Override
	public List<Fish> getOutTank(String tankId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		Tank tank = entityManager.find(Tank.class, tankId);
		if (tank != null) {
			CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
			Root<Fish> fRoot = query.from(Fish.class);
			query.where(cb.and(cb.isFalse(fRoot.get("discarded")), cb.or(
					cb.notEqual(fRoot.get("tank"), tank),
					cb.isNull(fRoot.get("tank"))
			)));
			List<Fish> resultList = entityManager.createQuery(query).getResultList();
			resultList.forEach(fish -> {
				entityManager.detach(fish);
				fish.setTank(null);
			});
			return resultList;
		} else {
			return null;
		}
	}
	
	@Override
	public List<Fish> getFromTank(String tankId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		Tank tank = entityManager.find(Tank.class, tankId);
		if (tank != null) {
			CriteriaQuery<Fish> query = cb.createQuery(Fish.class);
			Root<Fish> fRoot = query.from(Fish.class);
			query.where(cb.equal(fRoot.get("tank"), tank));
			List<Fish> resultList = entityManager.createQuery(query).getResultList();
			resultList.forEach(fish -> {
				entityManager.detach(fish);
				fish.setTank(null);
			});
			return resultList;
		} else {
			return null;
		}
	}
	
	@Override
	public void addGeneticCombination(KinshipLevel kinshipLevel) {
		entityManager.persist(kinshipLevel);
	}
	
	@Override
	public boolean deleteGeneticCombination(String combinationId) {
		KinshipLevel kinshipLevel = entityManager.find(KinshipLevel.class, combinationId);
		if (kinshipLevel != null) {
			entityManager.remove(kinshipLevel);
			return true;
		}
		return false;
	}
	
	@Override
	public List<KinshipLevel> getGeneticMatchbinations() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<KinshipLevel> query = cb.createQuery(KinshipLevel.class);
		Root<KinshipLevel> root = query.from(KinshipLevel.class);
		return entityManager.createQuery(query.select(root)).getResultList();
	}
	
	@Override
	public List<KinshipLevel> getGeneticMatchbinationsByFish(String fishId) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<KinshipLevel> query = cb.createQuery(KinshipLevel.class);
		Root<KinshipLevel> root = query.from(KinshipLevel.class);
		Fish fish = getById(fishId);
		query.where(cb.or(
				cb.and(cb.equal(root.get("fish1"), fish),
						cb.isFalse(root.get("fish2").get("discarded")),
						cb.notEqual(root.get("fish2").get("gender"), fish.getGender())
				),
				cb.and(cb.equal(root.get("fish2"), fish),
						cb.isFalse(root.get("fish1").get("discarded")),
						cb.notEqual(root.get("fish1").get("gender"), fish.getGender()))
		)).orderBy(cb.asc(root.get("level")));
		return entityManager.createQuery(query.select(root)).getResultList();
	}
	
	@Override
	public KinshipLevel getGeneticMatchbination(String tag1, String tag2) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<KinshipLevel> query = cb.createQuery(KinshipLevel.class);
		Root<KinshipLevel> root = query.from(KinshipLevel.class);
		query.where(
				cb.or(
						cb.and(cb.equal(root.get("fish1").get("tag"), tag1),
								cb.equal(root.get("fish2").get("tag"), tag2)),
						cb.and(cb.equal(root.get("fish1").get("tag"), tag2),
								cb.equal(root.get("fish2").get("tag"), tag1))
				));
		List<KinshipLevel> resultList = entityManager.createQuery(query.select(root)).getResultList();
		if (resultList != null && resultList.size() > 0) {
			return resultList.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public void updateKinshipLevel(KinshipLevel kinshipLevel) {
		KinshipLevel dbKinshipLevel = entityManager.find(KinshipLevel.class, kinshipLevel.getId());
		if (dbKinshipLevel != null) {
			dbKinshipLevel.setLevel(kinshipLevel.getLevel());
			entityManager.merge(dbKinshipLevel);
			entityManager.flush();
		}
	}
}
