package com.minetto.piscis_server.model.tank.dao;

import com.minetto.piscis_server.model.tank.Tank;

import java.util.List;

public interface TankDao {
	
	List<Tank> getAll();
	
	Tank getById(String id);
	
	Tank editTank(Tank tank);
	
	Tank addFish(String tankId, String fishId);
	
	Tank removeFish(String fishId);
	
	Tank createTank(Tank tank);
	
	boolean deleteTank(String tankId);
}
