package com.minetto.piscis_server.model.fish;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.minetto.piscis_server.model.tank.Tank;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Fish {
	
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	@Column
	private String tag;
	
	@Column
	private double size;
	
	@Column
	private double weight;
	
	@Column
	private int gender = 0;
	
	@Column
	private boolean discarded;
	
	@Column
	private String locality;
	
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	@Column(name = "date_capture")
	private Date dateCapture;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "reprodutive_history")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private List<Date> reprodutiveHistory;
	
	@Column
	private String species;
	
	@Column(unique = true)
	private String labId;
	
	@Column
	private String observation;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "tank_id")
	private Tank tank;
	
	public Fish() {
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public double getSize() {
		return size;
	}
	
	public void setSize(double size) {
		this.size = size;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public int getGender() {
		return gender;
	}
	
	public void setGender(int gender) {
		this.gender = gender;
	}
	
	public boolean isDiscarded() {
		return discarded;
	}
	
	public void setDiscarded(boolean discarded) {
		this.discarded = discarded;
	}
	
	public Tank getTank() {
		return tank;
	}
	
	public void setTank(Tank tank) {
		this.tank = tank;
	}
	
	public String getLocality() {
		return locality;
	}
	
	public void setLocality(String locality) {
		this.locality = locality;
	}
	
	public Date getDateCapture() {
		return dateCapture;
	}
	
	public void setDateCapture(Date dateCapture) {
		this.dateCapture = dateCapture;
	}
	
	public List<Date> getReprodutiveHistory() {
		if (reprodutiveHistory == null)
			return new ArrayList<>();
		reprodutiveHistory.sort(Comparator.reverseOrder());
		return reprodutiveHistory;
	}
	
	public void setReprodutiveHistory(List<Date> reprodutiveHistory) {
		this.reprodutiveHistory = reprodutiveHistory;
	}
	
	public String getSpecies() {
		return species;
	}
	
	public void setSpecies(String species) {
		this.species = species;
	}
	
	public String getLabId() {
		return labId;
	}
	
	public void setLabId(String labId) {
		this.labId = labId;
	}
	
	public String getObservation() {
		return observation;
	}
	
	public void setObservation(String observation) {
		this.observation = observation;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof Fish)) return false;
		return id.equalsIgnoreCase(((Fish) obj).id);
	}
	
	@Override
	public int hashCode() {
		return 42;
	}
}
